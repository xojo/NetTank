#tag Window
Begin Window CombatWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   400
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   220908098
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "NetTank"
   Visible         =   True
   Width           =   600
   Begin Canvas CombatCanvas
      AcceptFocus     =   True
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   True
      Enabled         =   True
      EraseBackground =   True
      Height          =   350
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   50
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   600
   End
   Begin Timer GameTimer
      Index           =   -2147483648
      InitialParent   =   ""
      LockedInPosition=   False
      Mode            =   2
      Period          =   10
      Scope           =   0
      TabPanelIndex   =   0
   End
   Begin PushButton StartButton
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "New Game"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   114
   End
   Begin Label Tank1ScoreLabel
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   169
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextAlign       =   0
      TextColor       =   &c0000FF00
      TextFont        =   "System"
      TextSize        =   32.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Tank2ScoreLabel
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   317
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextAlign       =   0
      TextColor       =   &cFF000000
      TextFont        =   "System"
      TextSize        =   32.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Timer ControllerTimer
      Index           =   -2147483648
      InitialParent   =   ""
      LockedInPosition=   False
      Mode            =   2
      Period          =   150
      Scope           =   0
      TabPanelIndex   =   0
   End
   Begin UDPSocket TankSocket
      Index           =   -2147483648
      LockedInPosition=   False
      Port            =   "#kPort"
      RouterHops      =   32
      Scope           =   2
      SendToSelf      =   False
      TabPanelIndex   =   0
   End
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   41
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   463
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Scope           =   2
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Use arrow keys to move, space to fire."
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   5
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   137
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  TankSocket.Port = kPort
		  TankSocket.Connect
		  
		  Self.Title = "NetTank (" + TankSocket.LocalAddress + ")"
		  
		  mInputManager = New GameInputManager
		  
		  StartGame
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub CheckController()
		  If mDevice = Nil Then
		    For i As Integer = 0 To mInputManager.DeviceCount - 1
		      If mInputManager.Device(i).Name.Left(7) = "Macally" Then
		        mDevice = mInputManager.Device(i)
		      End If
		    Next
		  End If
		  
		  If mDevice = Nil Then Return
		  
		  Dim controllerButton As GameInputElement
		  
		  For i As Integer = 0 To mDevice.ElementCount - 1
		    controllerButton = mDevice.Element(i)
		    
		    Select Case controllerButton.Name
		    Case "Button #1" // Forward
		      If controllerButton.Value = 1 Then
		        mTank1.Forward
		      End If
		    Case "Button #3" // Left
		      If controllerButton.Value = 1 Then
		        mTank1.RotateLeft
		      End If
		    Case "Button #4" // Right
		      If controllerButton.Value = 1 Then
		        mTank1.RotateRight
		      End If
		    Case "Button #5" // Fire
		      If controllerButton.Value = 1 Then
		        mTank1Missile = mTank1.Fire
		      End If
		    End Select
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub StartGame()
		  mTank1Score = 0
		  mTank2Score = 0
		  
		  mTank1Missile = Nil
		  mTank2Missile = Nil
		  
		  mTank1 = New Tank(50, 200, &c0000ff)
		  mTank2 = New Tank(500, 200, &cff0000)
		  
		  mTank2.RotateRight
		  mTank2.RotateRight
		  mTank2.RotateRight
		  mTank2.RotateRight
		  
		  CombatCanvas.Invalidate(False)
		  
		  IsPlayer1 = True
		  Dim b As Boolean
		  b = TankSocket.JoinMulticastGroup(kGroupAddress)
		  If Not b Then Break
		  
		  // Send join command to see if we are player 1 (first to join)
		  // or player 2 (second to join)
		  // If we are first to join they'll be no response so we'll stay as
		  // player 1.
		  Dim tankJSON As New JSONItem
		  tankJSON.Value("JoinAction") = "ready"
		  TankSocket.Write(kGroupAddress, tankJSON.ToString)
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private IsPlayer1 As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDevice As GameInputDevice
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mInputManager As GameInputManager
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTank1 As Tank
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTank1Missile As Missile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTank1Score As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTank2 As Tank
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTank2Missile As Missile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTank2Score As Integer
	#tag EndProperty


	#tag Constant, Name = kGroupAddress, Type = String, Dynamic = False, Default = \"225.1.4.2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kPort, Type = Double, Dynamic = False, Default = \"8355", Scope = Public
	#tag EndConstant


#tag EndWindowCode

#tag Events CombatCanvas
	#tag Event
		Function KeyDown(Key As String) As Boolean
		  If mTank1 Is Nil Then Return False
		  
		  // Keyboard controls for moving the tank
		  Dim k As Integer = Asc(Key)
		  
		  Dim tankJSON As New JSONItem
		  
		  Select Case Key
		    // Local Player 1 keys
		  Case Chr(30) // Up arrow
		    If IsPlayer1 Then
		      mTank1.Forward
		      tankJSON.Value("MoveAction") = Str(mTank1.X) + "," + Str(mTank1.Y) + "," + Str(mTank1.Rotation)
		    Else
		      mTank2.Forward
		      tankJSON.Value("MoveAction") = Str(mTank2.X) + "," + Str(mTank2.Y) + "," + Str(mTank2.Rotation)
		    End If
		    
		    TankSocket.Write(kGroupAddress, tankJSON.ToString)
		    
		  Case Chr(28) // Left arrow
		    // Rotate left
		    tankJSON.Value("MoveAction") = "left"
		    TankSocket.Write(kGroupAddress, tankJSON.ToString)
		    If IsPlayer1 Then
		      mTank1.RotateLeft
		    Else
		      mTank2.RotateLeft
		    End If
		    
		  Case Chr(29) // Right arrow
		    // Rotate right
		    tankJSON.Value("MoveAction") = "right"
		    TankSocket.Write(kGroupAddress, tankJSON.ToString)
		    If IsPlayer1 Then
		      mTank1.RotateRight
		    Else
		      mTank2.RotateRight
		    End If
		    
		  Case " " // Space = fire
		    tankJSON.Value("MoveAction") = "fire"
		    TankSocket.Write(kGroupAddress, tankJSON.ToString)
		    If IsPlayer1 Then
		      mTank1Missile = mTank1.Fire
		    Else
		      mTank2Missile = mTank2.Fire
		    End If
		    
		  End Select
		  
		  Return True
		End Function
	#tag EndEvent
	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  // Draws the tanks and missiles at the current positions.
		  
		  If mTank1 Is Nil Then Return
		  
		  g.ForeColor = &c00ff00
		  g.FillRect(0, 0, g.Width, g.Height)
		  
		  mTank1.Draw(g)
		  mTank2.Draw(g)
		  
		  If mTank1Missile <> Nil Then
		    mTank1Missile.Draw(g)
		  End If
		  
		  If mTank2Missile <> Nil Then
		    mTank2Missile.Draw(g)
		  End If
		  
		  // Update the score
		  Tank1ScoreLabel.Text = Str(mTank1Score)
		  Tank2ScoreLabel.Text = Str(mTank2Score)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events GameTimer
	#tag Event
		Sub Action()
		  // Moves missiles and checks if a missile
		  // has hit a tank.
		  
		  If mTank1 Is Nil Then Return
		  
		  If mTank1Missile <> Nil Then
		    // There is an active missile 1, so move it and then check
		    // if it has hit tank 2.
		    mTank1Missile.Move
		    
		    If IsPlayer1 And mTank2.IsHitBy(mTank1Missile) Then
		      // Send new coords to player2
		      Dim tankJSON As New JSONItem
		      tankJSON.Value("HitAction") = Str(mTank2.X) + "," + Str(mTank2.Y)
		      TankSocket.Write(kGroupAddress, tankJSON.ToString)
		      
		      mTank1Score = mTank1Score + 1
		      mTank1Missile = Nil
		      Return
		    End If
		    
		    // If the missile has reached the canvas border,
		    // remove it.
		    If mTank1Missile.X > CombatCanvas.Width Or _
		      mTank1Missile.X < 0 Or _
		      mTank1Missile.Y < 0 Or _
		      mTank1Missile.Y > CombatCanvas.Height Then
		      mTank1Missile = Nil
		    End If
		  End If
		  
		  If mTank2Missile <> Nil Then
		    // There is an active missile 2, so move it and then check
		    // if it has hit tank 1.
		    mTank2Missile.Move
		    
		    If Not IsPlayer1 And mTank1.IsHitBy(mTank2Missile) Then
		      // Send new coords to player1
		      Dim tankJSON As New JSONItem
		      tankJSON.Value("HitAction") = Str(mTank1.X) + "," + Str(mTank1.Y)
		      TankSocket.Write(kGroupAddress, tankJSON.ToString)
		      
		      mTank2Score = mTank2Score + 1
		      mTank2Missile = Nil
		      Return
		    End If
		    
		    // If the missile has reached the canvas border,
		    // remove it.
		    If mTank2Missile.X > CombatCanvas.Width Or _
		      mTank2Missile.X < 0 Or _
		      mTank2Missile.Y < 0 Or _
		      mTank2Missile.Y > CombatCanvas.Height Then
		      mTank2Missile = Nil
		    End If
		  End If
		  
		  // Tell the Canvas to update
		  CombatCanvas.Invalidate(False)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events StartButton
	#tag Event
		Sub Action()
		  StartGame
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ControllerTimer
	#tag Event
		Sub Action()
		  If mTank1 Is Nil Then Return
		  
		  CheckController
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TankSocket
	#tag Event
		Sub DataAvailable()
		  // Get the data containing Tank action
		  // Actions are: forward, left, right, fire
		  Dim data As Datagram = Me.Read
		  
		  Dim tankJSON As New JSONItem(data.Data)
		  
		  // Look for a JoinAction
		  If tankJSON.HasName("JoinAction") Then
		    Dim joinAction As String = tankJSON.Value("JoinAction")
		    Select Case joinAction
		    Case "ready"
		      // Tell 2nd client that it has to be player 2
		      Dim outJSON As New JSONItem
		      outJSON.Value("JoinAction") = "player2"
		      TankSocket.Write(kGroupAddress, outJSON.ToString)
		    Case "player2"
		      IsPlayer1 = False
		    End Select
		  End If
		  
		  // Look for a MoveAction
		  If tankJSON.HasName("MoveAction") Then
		    Dim moveAction As String = tankJSON.Value("MoveAction")
		    
		    Select Case moveAction
		    Case "forward"
		      If IsPlayer1 Then
		        mTank2.Forward
		      Else
		        mTank1.Forward
		      End If
		    Case "left"
		      If IsPlayer1 Then
		        mTank2.RotateLeft
		      Else
		        mTank1.RotateLeft
		      End If
		    Case "right"
		      If IsPlayer1 Then
		        mTank2.RotateRight
		      Else
		        mTank1.RotateRight
		      End If
		    Case "fire"
		      If IsPlayer1 Then
		        mTank2Missile = mTank2.Fire
		      Else
		        mTank1Missile = mTank1.Fire
		      End If
		    Case Else
		      // Tank is moving forward
		      // The actual coordinates and rotation 
		      // are sent here to help keep things in sync
		      Dim values() As String = moveAction.Split(",")
		      
		      If IsPlayer1 Then
		        mTank2.X = Val(values(0))
		        mTank2.Y = Val(values(1))
		        mTank2.Rotation = Val(values(2))
		      Else
		        mTank1.X = Val(values(0))
		        mTank1.Y = Val(values(1))
		        mTank1.Rotation = Val(values(2))
		      End If
		    End Select
		  End If
		  
		  // Look for a HitAction
		  If tankJSON.HasName("HitAction") Then
		    Dim hitAction As String = tankJSON.Value("HitAction")
		    Dim values() As String = hitAction.Split(",")
		    If IsPlayer1 Then
		      // Player 1 was hit by player 2
		      mTank1.X = Val(values(0))
		      mTank1.Y = Val(values(1))
		      
		      // Clear player 2 missile and update their score
		      mTank2Missile = Nil
		      mTank2Score = mTank2Score + 1
		    Else
		      // Player 2 was hit by player 1
		      mTank2.X = Val(values(0))
		      mTank2.Y = Val(values(1))
		      
		      // Clear player 1 missile and update their score
		      mTank1Missile = Nil
		      mTank1Score = mTank1Score + 1
		    End If
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub Error()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
#tag EndViewBehavior
