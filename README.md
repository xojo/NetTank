# NetTank

NetTank is a networked version of Combat, the old Atari 2600 game with two tanks on the screen shooting at each other. Made for the 2008 Xojo Just Code Challenge, NetTank uses UDP for sending messages between the running app clients.