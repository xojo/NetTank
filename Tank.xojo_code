#tag Class
Protected Class Tank
	#tag Method, Flags = &h0
		Sub Constructor(x As Integer, y As Integer, c As Color)
		  Self.X = x
		  Self.Y = y
		  
		  TankColor = c
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Draw(g As Graphics)
		  // Draw the tank
		  // ===
		  //  =---
		  // ===
		  
		  Dim tankPic As New Picture(kSize, kSize)
		  
		  tankPic.Graphics.ForeColor = TankColor
		  tankPic.Graphics.FillRect(0, tankPic.Graphics.Height - 10, _
		  tankPic.Graphics.Width - 10, 10)
		  
		  tankPic.Graphics.FillRect(0, 0, tankPic.Graphics.Width - 10, 10)
		  
		  tankPic.Graphics.FillRect(10, 10, 15, 20)
		  
		  tankPic.Graphics.FillRect(25, 19, 15, 4)
		  
		  // Create a Vector shape for the tank bitmap so
		  // that it can be rotated later.
		  Dim s As New PixmapShape(tankPic)
		  
		  // Set the center of the image for rotation
		  s.X = kSize \ 2
		  s.Y = kSize \ 2
		  
		  // Rotation is always specified in degrees, but vector
		  // graphics need it converted to radians.
		  // pi/180 = 0.01745329251
		  s.Rotation = mRotation * 0.01745329251
		  
		  g.DrawObject(s, X, Y)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Fire() As Missile
		  // Create a missile in the center of the tank
		  
		  Dim m As New Missile(Self.X, Self.Y, mRotation)
		  
		  FireSound.Play
		  
		  Return m
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Forward()
		  // Move the tank forward
		  
		  Const kSpeed = 5 // pixels to move
		  
		  Select Case mRotation
		  Case 0, 360
		    X = X + kSpeed
		  Case 45
		    X = X + kSpeed
		    Y = Y + kSpeed
		  Case 90
		    Y = Y + kSpeed
		  Case 135
		    X = X - kSpeed
		    Y = Y + kSpeed
		  Case 180
		    X = X - kSpeed
		  Case 225
		    X = X - kSpeed
		    Y = Y - kSpeed
		  Case 270
		    Y = Y - kSpeed
		  Case 315
		    X = X + kSpeed
		    Y = Y - kSpeed
		  End Select
		  
		  If Y < 0 Then Y = 0
		  If X < 0 Then X = 0
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsHitBy(m As Missile) As Boolean
		  // If missile hits the tank, then move it to another location
		  
		  Dim tankRect As New Realbasic.Rect(X, Y, kSize, kSize)
		  Dim missileRect As Realbasic.Rect = m.Rect
		  
		  If tankRect.Intersects(missileRect) Then
		    X = App.Randomizer.InRange(40, 560)
		    Y = App.Randomizer.InRange(40, 310)
		    
		    HitSound.Play
		    
		    Return True
		  Else
		    Return False
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RotateLeft()
		  // Rotate the tank left by 45 degrees
		  
		  If mRotation < 45 Then mRotation = 360
		  
		  mRotation = mRotation - 45
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RotateRight()
		  // Rotate the tank right by 45 degrees
		  
		  If mRotation > 315 Then mRotation = 0
		  
		  mRotation = mRotation + 45
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mRotation As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mRotation
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRotation = value
			End Set
		#tag EndSetter
		Rotation As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		TankColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		X As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Y As Integer
	#tag EndProperty


	#tag Constant, Name = kSize, Type = Double, Dynamic = False, Default = \"40", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="X"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Y"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TankColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
